import fs from 'fs';
import path from 'path';
import { channelize, colorize, nickname } from "./helpers/decorate";

class Logger {
    constructor(config = {}) {
        this._config = config;
    }

    update(config = {}) {
        this._config = config;
    }

    static filename(channel) {
        return channel + new Date().toLocaleString().replace( /(\d{2}).(\d{2}).(\d{4}), .*/, '_$3$2$1.log' );
    }

    message(msg, color, channel) {
        let string;
        let rawString;
        if (typeof msg === 'string') {
            string = Logger.time() + ( color ? colorize( msg, color ) : msg );
            rawString = Logger.time( false ) + msg;
        } else {
            string = Logger._parse( msg );
            rawString = Logger._parse( msg, false );
            color = undefined;
        }

        if (this._config.logging !== false) {
            this._log( rawString, channel );
        }

        if (this._config.display !== false) {
            Logger._print( string, color )
        }
    }

    error(string) {
        this.message( '##### ' + string + ' #####', 'Red', 'error' );
    }

    _log(string, channel = 'log') {
        fs.writeFileSync(
            path.join( ( this._config.prefix || '' ), Logger.filename( channel ) ),
            string + '\n',
            { flag: 'a' }
        )
    }

    static _print(string, color) {
        console.log( color ? colorize( string, color ) : string );
    }

    static time(color = true) {
        const time = new Date().toLocaleString().replace( /.*,\s(.*)/, '$1' );
        return ( color ? colorize( time, 'Yellow' ) : time ) + ' | ';
    }

    static _parse(msg, color = true) {
        const username = nickname( msg?.tags?.displayName, 'TWITCH' );
        let result;

        if (msg.command === 'JOIN') {
            const joinString = ' присоединился к каналу';
            const name = nickname( msg.username, 'empty' );
            result = color ?
                channelize( msg.channel ) + colorize( name, 'Green' ) + colorize( joinString, 'Red' )
                :
                `${channelize( msg.channel, false )}${name} ${joinString}`;
        } else if (msg.command === 'PART') {
            const partString = ' отключился от канала';
            const name = nickname( msg.username, 'empty' );
            result = color ?
                channelize( msg.channel ) + colorize( name, 'Green' ) + colorize( partString, 'Red' )
                :
                `${channelize( msg.channel, false )}${name} ${partString}`;
        } else if (!!msg.message) {
            result = color ?
                channelize( msg.channel ? msg.channel: ' ' ) +
                colorize( username, "Green" ) +
                ' ' +
                msg.message
                :
                `${msg.channel} ${username} ${msg.message}`;
        } else {
            result = color ?
                `${channelize( '#twitch' )}${colorize( username + ' ' + msg._raw, 'Black' )}`
                :
                `${channelize( '#twitch', false )}${username} ${msg._raw}`;
        }

        return this.time( color ) + result;
    }
}

export default Logger;