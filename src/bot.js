import { wait } from "./helpers/utility";
import { channelize, colorize } from "./helpers/decorate";

class Bot {
    waiting = {};
    scope = {};
    _lastAnswer = '';
    _lastAnswerRepeating = 0;

    constructor(chat, store, commands, logger) {
        this.say = (channel, message) => chat.say( channel, message );
        this.whisper = (username, message) => chat.whisper( username, message );
        this.join = (channel) => chat.join( channel );
        this.part = (channel) => chat.part( channel );
        this._commands = [ ...commands ];
        this._store = store;
        this._logger = logger;
    }

    parse = (msg) => ( {
        username: msg.tags?.displayName,
        message: msg.message,
        channel: msg.channel,
        isSubscriber: msg.tags?.isSubscriber,
        isModerator: msg.tags?.isModerator
    } );

    setCommands(commands) {
        this._commands = [ ...commands ];
    }

    _russify(string) {
        const en = 'eyopaxcmbh';
        const rus = 'еуорахсмвн';

        return en.split( '' ).reduce( (accumulator, char, index) => accumulator.replace( new RegExp(char, 'gi'), rus[ index ] ), string )
    }

    test = (commandString, message) => {
        if (typeof commandString === 'string') {
            if (!message || 
                (!this._russify( message?.toLowerCase() || '' ).startsWith( '!' + commandString ) &&
                !(commandString.startsWith('@') && message.toLowerCase().startsWith( commandString )))
               ) {
                return false;
            }
        } else {
            let isMatch;
            for (const command of commandString) {
                isMatch = this._russify( message?.toLowerCase() || '' ).startsWith( '!' + command );

                if (isMatch) {
                    break
                }
            }

            if (!isMatch) {
                return false;
            }
        }

        return true;
    };

    filter = (condition, info) => {
        const type = typeof condition;
        if (type === 'function') {
            return condition( info )
        } else if (type === 'object') {
            return Object
                .entries( condition )
                .reduce( (accumulator, [ key, value ]) => accumulator && info[ key ] === value, true )
        }

        return true;
    };

    executeAction = (action, info) => {
        const type = typeof action;
        if (type === 'function') {
            return action( info, this._store, this._logger, this );
        } else if (type === 'string') {
            return Object
                .entries( info )
                .reduce( (accumulator, [ key, value ]) => accumulator.replace( '${' + key + '}', value ), action )
        }
    };

    async message(channel, username, answer, timeout = 1, whisper = false) {
        if (!this.waiting[ channel ]) {
            this.waiting[ channel ] = true;
            try {
                await this.say( channel, answer );
            } catch (e) {
            }
            await wait( timeout );
            this.waiting[ channel ] = false;
        } else if (whisper) {
            await this.whisper( username, answer );
        }
    }

    wrapper = ({ command, action, condition, timeout = 1, whisper = false }) => async (info) => {
        const { username, message, channel } = info;

        if (!this.test( command, message ) || !this.filter( condition, info )) {
            return false;
        }

        let answer;
        let next;
        let scope;
        if (!this.waiting[ channel ] || whisper) {
            answer = this.executeAction( action, info );

            if (typeof answer === 'object' && !answer.result) {
                return !answer.next;
            }

            next = answer.next;
            scope = answer.scope;
            answer = answer.result || answer;

            this._logger.message( `${channelize( channel )}${colorize( '<BOT>', 'Cyan' )} ${answer}` );

            if (answer === this._lastAnswer) {
                this._lastAnswer = answer;
                answer += ` [${++this._lastAnswerRepeating}]`;
            } else {
                this._lastAnswer = answer;
                this._lastAnswerRepeating = 0;
            }
        }

        await this.message( channel, username, answer, timeout, whisper );

        if(scope) {
            this.scope[ channel ] = scope;
        }

        return !next;
    };

    async executeScopeAction(info) {
        const { username, channel } = info;

        if (this.scope[ channel ]) {

            const { out, result, scope } = this.scope[ channel ].action( info, this._store, this._logger, this );

            if(result) {
                await this.message( channel, username, result, this.scope[ channel ].timeout, this.scope[ channel ].whisper );
            }

            if (out) {
                this.scope[ channel ] = null;
                return false;
            }

            this.scope[ channel ] = scope;
            return true;
        } else {
            return false;
        }
    }

    async execute(msg) {
        if (!msg) {
            return;
        }

        const info = this.parse( msg );
        if (await this.executeScopeAction( info )) {
            return;
        }

        for (const command of this._commands) {
            if (await this.wrapper( command )( info )) {
                return;
            }
        }
    }
}

export default Bot;