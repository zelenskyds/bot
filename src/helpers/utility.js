export async function wait(seconds) {
    let resolve;
    const promise = new Promise( (r) => resolve = r );

    // noinspection JSUnusedAssignment
    setTimeout(
        resolve,
        1000 * seconds
    );

    return promise;
}