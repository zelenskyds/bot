const colors = {
    "Black": 0,
    "Red": 1,
    "Green": 2,
    "Yellow": 3,
    "Blue": 4,
    "Magenta": 5,
    "Cyan": 6,
    "White": 7,
};

const padCenter = (string, length, symbol = ' ') => {
    if (string.length === length) {
        return string;
    } else if (string.length > length) {
        return string.slice( 0, length - 3 ) + '...';
    } else {
        const pad = length - string.length;
        const odd = pad % 2;
        let leftPad = Math.floor( pad / 2 );
        let rightPad = leftPad;
        if (odd) {
            rightPad += 1;
        }

        return symbol.repeat( leftPad ) + string + symbol.repeat( rightPad );
    }
};

export function colorize(string, color, bold=1) {
    let colorString;
    if (typeof color === 'string') {
        colorString = (30 + colors[ color ]) || 37;
    } else {
        colorString = color.map( (c, i) => ( i * 10 + 30 + colors[ c ] ) || 37 ).join( ';' );
    }


    return `${String.fromCharCode( 27 )}[${bold};${colorString}m${string}${String.fromCharCode( 27 )}[0m`;
}

export function nickname(name, placeholder) {
    return `\<${name || placeholder}\>`;
}

export function channelize(channel, color = true, length = 14) {
    if (!channel) {
        return '';
    }
    return ( color ? colorize( padCenter( channel, length ), "Magenta" ) : channel ) + ' | ';
}