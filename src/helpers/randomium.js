export const random = (min, max) => Math.floor(Math.random() * (++max - min) ) + min;

export const choose = (array) => array[ random( 0, array.length - 1 ) ];

