import fs from 'fs';
import path from 'path';

const SAVE_PATH = 'bot.json';

class Store {
    constructor(currentPath, logger, filePath = SAVE_PATH) {
        this._currentPath = path.join( currentPath, filePath );
        this._logger = logger;
        this.load();
    }

    load() {
        try {
            this._data = JSON.parse( fs.readFileSync( this._currentPath ) );
        } catch (e) {
            console.log( e );
            this._data = {};
        }
    }

    save() {
        try {
            return fs.writeFileSync( this._currentPath, JSON.stringify( this._data ) );
        } catch (e) {
            this._logger.message( e.message );
        }
    }

    get(key) {
        return this._data[key];
    }

    set(key, value) {
        if(typeof key === 'string') {
            this._data[key] = value;
        } else {
            this._data = {
                ...this._data,
                ...key
            }
        }
    }
}

export default Store;