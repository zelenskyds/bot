import TwitchJs from "twitch-js";
import path from 'path';

import { ADMIN, TOKEN, USERNAME } from './config';
import Bot from "./bot"
import commands from "./commands";
import Store from "./store";
import Logger from "./logger";

export default function (currentPath) {
    const { chatConstants, chat } = new TwitchJs( { token: TOKEN, username: USERNAME } );
    const logger = new Logger( { prefix: path.join(currentPath, 'logs') });
    const store = new Store( currentPath, logger );
    const bot = new Bot( chat, store, commands, logger );

    chat.on( chatConstants.EVENTS.ALL, async (msg) => {
        logger.message( msg );

        try {
            await bot.execute( msg );
        } catch (e) {
            logger.error( e.message )
        }

        store.save();
    } );

    chat.connect().then( () => {
        for (const channel of [...(store.get('channels') || []), `#${USERNAME}`, `#${ADMIN}`]) {
            chat.join( channel );
        }
    } );
}