import pluralize from 'pluralize-ru';

import { ADMIN, USERNAME } from "./config";
import { choose, random } from "./helpers/randomium";
import fWords from './dictionaries/f';
import { channelize, colorize } from "./helpers/decorate";

export default [
	{
        command: '',
        action: function toggle({ channel, username, message }, store) {
            let result = '';
            let enabled = store.get( 'enabled' );
            if (enabled === undefined) {
                enabled = { [ channel ]: true };
            } else if (enabled[ channel ] === undefined) {
                enabled[ channel ] = true;
            }

            if (username === ADMIN || username === channel.slice( 1 )) {
                if (message === '!включить') {
                    result = `@${username}, бот теперь включён!`;
                    enabled[ channel ] = true;
                } else if (message === '!выключить') {
                    result = `@${username}, бот теперь выключен!`;
                    enabled[ channel ] = false;
                }
            }

            store.set( {
                enabled
            } );

            return { result, next: enabled[ channel ] };
        },
        whisper: true,
    },
	{
		command: '@quyabot',
		action: ({ username, message, channel, isSubscriber }) => {
			if(message.search(/(привет|здоров|здравствуй|хай|hi|hello|ку|кк|кукусики|прив)/i) === -1) {
				return { next: true, result: '' }
			}

			if(username === ADMIN) {
				return `@${username}, привет, папа!`;
			} else if(!isSubscriber) {
				return `@${username}, ансабы умеют разговаривать? DansGame `;
			} else if(username.toLowerCase() === 'k_i_ra') {
				return `@${username}, привет самому лучшему стримеру! *карась мод офф*`;
			} else if('#' + username.toLowerCase() === channel.toLowerCase()) {
				return `@${username}, привет, стример!`;
			} else if(username.toLowerCase() === 'l33thall') {
				return `@${username}, привет, дядя!`;
			} else if(username.toLowerCase() === 'fynjyei') {
				return `@${username}, привет, ф... фу... как там тебя сегодня зовут?! Кто-нибудь, пожалуйста, проверьте с помощью '!ф'`;
			} else if(username.toLowerCase() === 'i_k_ar') {
				return `@${username}, привет, ноунейм-модер!`;
			} else if(username.toLowerCase() === 'skinnid') {
				return `@${username}, привет, токсик!`;
			} else if(username.toLowerCase() === 'sveily') {
				return `@${username}, привет, тупая деффка! Ой, папа, не бей, я больше не буду! Приветствую Вас, леди Свейли!`;
			}

			return `@${username}, привет, великолепный саб!`;
 		}
	},
    {
        command: 'лапки',
        action: 'onyakaLapa onyakaLapa onyakaLapa',
        condition: { channel: '#onyaka' }
    },
	{
        command: ['карась', 'ikарaсв'],
        action: () => null,
        condition: { channel: '#dakryona' }
	},
    {
        command: '!голосование',
        action: function vote({ channel, username, message }, store, logger, bot) {
            const scope = bot.scope[ channel ];
            const variants = scope.variants;
            const users = scope.users;
            message = message.toLowerCase();
            username = username.toLowerCase();
            console.log( 1 );

            if (message.startsWith( '!голосование' ) && username === ADMIN) {
                console.log( 1 );
                const [ _, type, title ] = message.match( /!голосование\s+(завершить)?\s*["']?(.+)["']?/gi );
                if (type === 'завершить' && scope) {
                    const values = Object.entries( variants );
                    values.sort( (a, b) => b - a );

                    const res = values.slice( 0, 3 ).map( ([ v, w ]) => `"${v}": ${pluralize(
                        w,
                        '%d голосов',
                        '%d голос',
                        '%d голоса',
                        '%d голосов',
                    )}` ).join( ', ' );

                    return {
                        out: true,
                        result: `Голосование "${scope.title}" завершено. Топ-${values.slice( 0, 3 ).length}: ${ res }.`
                    };
                } else {
                    return {
                        scope: {
                            action: vote,
                            variants: {},
                            users: [],
                            title
                        },
                        result: `Начато голосование "${title}"`
                    }
                }

            } else if (users.indexOf( username ) === -1) {
                if (!variants[ message ]) {
                    variants[ message ] = 1;
                } else {
                    variants[ message ]++;
                }
            }

            return {
                scope: {
                    ...scope,
                    variants
                },
            }
        },
        whisper: false
    },
    {
        command: 'телега',
        action: '@${username} Моя телега — мой лучший друг. ' +
            'Она — моя жизнь. Я должен научиться владеть ' +
            'ею так же, как я владею своей жизнью. Без меня ' +
            'моя телега бесполезна. Без моей телеги бесполезен ' +
            'я. Я должен стрелять из моей телеги метко. Моя ' +
            'телега должна стрелять точнее чем враг, который ' +
            'пытается убить мою телегу.',
        whisper: true
    },
    {
        command: 'летал',
        action: '@${username}, Kreygasm Kreygasm Kreygasm Kreygasm Kreygasm ' +
            'Летал красивый, скромный, умный Kreygasm Kreygasm Kreygasm Kreygasm ' +
            'порой он правда чуть-чуть буйный Kreygasm ' +
            'ответственный модер он также вдобавок Kreygasm Kreygasm Kreygasm ' +
            'а цвет его в чате немерено ярок Kreygasm ',
        condition: { username: ADMIN, message: '!летал' }
    },
    {
        command: 'летал',
        action: ({ username }, store) => {
            let votedPeople = store.get( 'votedPeople' );

            if (!votedPeople) {
                votedPeople = [];
            }

            if (votedPeople.indexOf( username ) === -1) {
                votedPeople = [ ...votedPeople, username ];
                store.set( { votedPeople } );
                return `@${username} голосует за выдвижение @l33thall в президенты! Уже проголосовало ${votedPeople.length + 19276927692765600}!`;
            } else {
                return `@${username}, ты уже голосовал за выдвижение @l33thall в президенты! Уже проголосовало ${votedPeople.length + 19276927692765600}!`
            }

        },
        whisper: true
    },
    {
    	command: '!ф',
    	action: 'Фундук имеет очень скудный склад ума BrokeBack' + 
    			' В его голове глупых мыслей полнющая тьма BrokeBack' + 
    			' Но бунтовать это ему не мешает BrokeBack' + 
    			' Каждый от злости его пострадает',
		whisper: true,
		timeout: 10
    },
    {
        command: '!карась',
        action: '@${username} Карась в нашем мире как рыба ' +
            'в воде SabaPing Ходят легенды о его доброте ' +
            'SabaPing В поисках дамы он потерялся SabaPing ' +
            'но ни с кем не целовался SabaPing',
        whisper: true
    },
    {
        command: 'карась',
        action: ({ username }) => `@${username} Любвеобильный @iKapacb сегодня задонатил ` + pluralize(
            random( 10, 100 ),
            "",
            '%d стримерше!',
            '%d стримершам!',
            '%d стримершам!',
        ),
        whisper: true
    },
    {
        command: 'dizе',
        action: '@${username}, ты и есть dize!',
        condition: ({ channel, username }) => channel !== '#k_i_ra' && channel !== '#'+ADMIN && username === ADMIN
    },
    { command: 'гусь', action: 'Гусь не смеет цапать Дайза!', condition: { username: ADMIN } },
    {
        command: [ 'гусь', 'ф', 'револьвер', 'подкатить к', 'снежок', 'снеговик' ],
        action: '@${username}, эта команда только для великолепных сабов!',
        condition: ({ username, channel, isSubscriber }) => !isSubscriber && channel === '#k_i_ra' && username.toLowerCase() !== 'elisey_est_goosey'
    },
    {
    	command: 'снеговик',
    	action: '############################### kupaShapka ###################################### kupaSnegovik ###################################### kupaSnegovik2 ###################################### kupaSnegovik2'
    },
    {
        command: 'подкатить к',
        action: ({ username, message }) => {
            const regex = /!подкатить к\s+(.*)/gi;
            let [_, subject] = regex.exec(message) || [];

            if(!subject) {
                return { next: true, result: '' }
            }

            if(subject.startsWith('@')) {
                subject = subject.slice(1);
            }
            let result;

            if(username === ADMIN) {
                result = `@${subject}, к тебе подкатывает великолепный и несравненный Дайз! Он весьма красив, очень богат и бесконечно умен! Соглашайся на все, не думай!`;
            } else {
                result = `@${subject}, к тебе подкатывает @${username}! Возможно он красив, богат и умен, но это не точно! Что ответишь?`;
            }

            return result;
        }
    },
    {
        command: 'снежок',
        action: ({ username, message, channel }) => {
            const regex = /!снежок\s+([^\s]*)/gi;
            let [_, subject] = regex.exec(message) || [];

            if(!subject) {
                return { next: true, result: '' }
            }

            subject = subject.toLowerCase();

            if(subject.startsWith('@')) {
                subject = subject.slice(1);
            }
            let result;

            if(username === ADMIN || username === 'l33thall') {
                result = `@${subject}, тебе в лицо прилетел снежок! Как тебе такое, а?`;
            } else {
            	const variants = [
            		`@${username} кидает снежок в @${subject}, но видимо @${username} имеет небольшое косоглазие, и поэтому снежок никуда не попадает, а @${subject} громко и обидно хохочет!`,
            		`@${username} кидает снежок в @${subject}. @${username} скрывается с места преступления с улыбкой до ушей!`,
            		`@${username} метко кидает снежок в @${subject} и попадает ему в лицо. @${subject}, вкусный снег в этом году?`,
            		`@${username} коварно подкрадывается со снежком к @${subject} и засовывет пригорошню снега прямо за шиворот! Вот уж не ждали мы от тебя, @${username}, такой подлости!`,
            		`@${username} кидает снежок, но @${subject} мастерстки ловит его на лету и кидает в обратную сторону! Нет, ну вы это видели?`,
            		`@${username} кидает снежок, но неклюже поскальзывается и падает прямо в сугроб. Видимо, сегодня неудачный день!`,
            	]
           
                result = variants[subject === ADMIN || '#' + subject === channel || subject === 'l33thall'? 0: random(0, variants.length - 1)];
            }

            return result;
        },
        timeout: 10
    },
    {
        command: 'dizе',
        action: ({ username }) => pluralize(
            random( 1, 100 ),
            '',
            `@${username}, ты равен по крутости великолепному Дайзу!`,
            `@${username}, ты хуже чем великолепный Дайз в %d раза!`,
            `@${username}, ты хуже чем великолепный Дайз в %d раз!`,
        ),
        whisper: true,
        condition: ({ channel }) => channel !== '#k_i_ra' && channel !== '#'+ADMIN  
    },
    { command: 'гусь', action: '@BerryGoose_ цапает за жёпку @${username}', whisper: true },
    { command: 'ф', action: () => `@fynjyei, теперь ты "${choose( fWords )}"`, whisper: true },
    { command: 'гей', action: '@${username} хочет быть геем! Натуралы, бейте его!' },
    {
        command: 'револьвер',
        action: function revolver({ username, channel }, _, logger) {
            let answer = '';

            if(username.toLowerCase() === 'deepcosmo1') {
            	return `${username}, леди, я не позволю Вам умереть!`;
            }

            if (!revolver[ channel ]) {
                revolver[ channel ] = {
                    bulletIndex: random( 0, 5 ),
                    index: 0
                };

                logger.message(
                    `${
                        channelize( channel )
                        }${
                        colorize( `>>> (РЕВОЛЬВЕР) ${revolver[ channel ].bulletIndex + 1}й выстрел убьет.`, 'Blue' )
                        }`
                );

                answer = `@${username} играет в русскую рулетку. ` +
                    `Шестизарядный револьвер заряжен одним патроном, ` +
                    `барабан прокручен наугад. `;
            }

            answer += `@${username} нажимает на курок. `;
            if (revolver[ channel ].index === revolver[ channel ].bulletIndex) {
                answer += `Раздается выстрел, @${username} падает замертво. Это конец.`;
                revolver[ channel ] = null;
            } else {
                answer += `Пули в патроннике не оказывается! Фух, @${username} ` +
                    `выживает. Это был ${revolver[ channel ].index + 1}й выстрел. ` +
                    'Кто следующий смельчак?';
                revolver[ channel ].index++;
            }

            return answer;
        },
        timeout: 10
    },
    {
        command: 'оняка поцелуй',
        action: '@${username}, Оняка посылает тебе поцелуй! BrokeBack',
        condition: { username: ADMIN }
    },
    {
        command: 'оняка список',
        action: '@${username}, ' +
            '<1: Оняка лллычит! onyakaLapa > | ' +
            '<2: Оняка кавайно смущается! onyakaLapa > | ' +
            '<3: Оняка не знает чего хочет! onyakaLapa > | ' +
            '<4: Оняка злится и мило булчит! onyakaLapa > | ' +
            '<5: Оняка звонко смеется! onyakaLapa > | ' +
            '<6: Оняка довольно улыбается! onyakaLapa >',
        condition: { channel: '#onyaka' }
    },
    {
        command: 'оняка',
        action: ({ username, message }) => {
            const variants = [
                'Оняка лллычит! onyakaLapa',
                'Оняка кавайно смущается! onyakaLapa',
                'Оняка не знает чего хочет! onyakaLapa',
                'Оняка злится и мило булчит! onyakaLapa',
                'Оняка звонко смеется! onyakaLapa',
                'Оняка довольно улыбается! onyakaLapa'
            ];

            const match = message.match( /!оняка\s(\d)/i );
            let result;
            if (match && ( match[ 1 ] - 1 ) < variants.length) {
                result = variants[ match[ 1 ] - 1 ];
            } else {
                result = choose( variants );
            }

            return `@${username}, ${result}`;
        },
        condition: { channel: '#onyaka' },
        whisper: true
    },
    {
        command: 'подключить',
        action: ({ username, message }, store, logger, bot) => {
            let [ cmd, channel ] = message.replace( /\s+/, ' ' ).split( ' ' );
            const channels = store.get( 'channels' ) || [];

            if (!channel) {
                return `@${username}, нужно указать канал! (!подключить <канал>)`;
            } else if (channel.startsWith( '@' )) {
                channel = '#' + channel.slice( 1 );
            } else if (!channel.startsWith( '#' )) {
                channel = '#' + channel;
            }

            if (channels.indexOf( channel ) !== -1) {
                return `@${username}, этот канал уже подключён!`;
            }

            store.set( {
                channels: [ ...channels, channel ]
            } );

            bot.join( channel );

            return `@${username}, ты подключил канал ${channel}!`;
        },
        condition: { username: ADMIN }
    },
    {
        command: 'отключить',
        action: ({ username, channel }, store, logger, bot) => {
            const channels = [ ...( store.get( 'channels' ) || [] ) ];
            const channelIndex = channels.indexOf( channel );

            if (channel === `#${ADMIN}` || channel === `#${USERNAME}`) {
                return `@${username}, нельзя отключить ${channel}!`
            }

            channels.splice( channelIndex, 1 );

            store.set( {
                channels
            } );
            bot.part( channel );

            return `@${username}, ты отключил канал ${channel}!`
        },
        condition: { username: ADMIN }
    },
];